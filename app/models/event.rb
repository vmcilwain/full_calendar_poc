class Event < ApplicationRecord
  belongs_to :user
  include ActiveModel::Validations
  validates_with EventConflict
end
