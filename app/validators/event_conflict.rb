class EventConflict < ActiveModel::Validator
  def validate(record)
    events = Event.where("user_id= ? AND start_time between ? AND ? AND end_time between ? AND ?", record.user_id, record.start_time, record.end_time, record.start_time, record.end_time)
    if events.any?
      record.errors[:base] << "Bartender not available"
      false
    end
  end
end
